With vRealize Easy Installer, you can:
•	Install vRealize Suite Lifecycle Manager
•	Deploy VMware Identity Manager and register with vRealize Automation
•	Install a new instance of vRealize Automation
vRealize Easy Installer supports
•	VMware Identity Manager 3.3.1
•	vRealize Automation 8.0.0
